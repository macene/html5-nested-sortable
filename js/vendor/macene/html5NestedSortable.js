/*
 * HTML5 Nester Sortable
 * v 1.0.0-alpha / 15 dec 2012
 * http://manuelcendese.com
 * https://bitbucket.org/macene/html5-nested-sortable/src
 *
 * Copyright (c) 2012 Manuel Cenedese
 * Licensed under the MIT License
 * http://www.opensource.org/licenses/mit-license.php
 */

(function( $ ){

    var methods = {

	    delete : function( opts )
	    {
            console.log($(this));
	    },
        init : function( opts )
        {
            var options;
            var defaults = {
	            data: '{}',
                items: 'li'
            };
            options = $.extend(defaults, opts);

	        var items;
            var container = $(this);
            var placeholder,  finalTarget;
            var containerData, mouseStartData, mouseData;
            var ulMargin = 20;
	        var targetId;


	        /*
	         * knockout behaviours
	         */
	        var Leaf = function(id, title, noDelete, noAdd, noEdit, children) {
	            this.id = ko.observable(id);
	            this.title = ko.observable(title);
                this.noDelete = ko.observable(noDelete);
                this.noEdit = ko.observable(noEdit);
                this.noAdd = ko.observable(noAdd);
	            this.children = ko.observableArray(children || []);
	        };

	        var TreeModel = function() {
	            this.tree = ko.mapping.fromJS(options.data);

	            this.addChild = function(id, title, noDelete, noAdd, noEdit, parentArray) {
	                parentArray.push(new Leaf(id, title, noDelete, noAdd, noEdit));
	            };

		        this.removeChild = function(child, parentArray) {
                    parentArray.remove(child);
                };
	        };

	        ko.applyBindings(new TreeModel());
	        /*
	         * end knockout behaviours
	         */



	        container.on('dragstart touchstat', 'li', function(e) {
		        e.stopPropagation();
	            //e.preventDefault();

                placeholder = $(this);
		        placeholderId = placeholder.attr('id');
                placeholderLevel = placeholder.parentsUntil(container,'li').length;

		        var dt = e.originalEvent.dataTransfer;
                dt.effectAllowed = 'move';
                dt.setData('text/plain', placeholderId);

                containerData = getItemData(container);
                mouseStartData = getMouseData(e);
                placeholder.addClass('placeholder');
                items = container.find(options.items).not('.placeholder').not('.placeholder li');
            }).on('dragend drop touchend', function(e){
			        e.preventDefault();
			        placeholder.removeClass('placeholder');
	        }).on('dragover dragenter touchmove', function(e) {
                e.stopPropagation();
		        e.preventDefault();
		        e.originalEvent.dataTransfer.dropEffect = 'move';

                // get items y position
                itemsY = {};
                items.each(function(){
                    itemsY[$(this).attr('id')] = $(this).position().top;
                });

	            // get mouse y position
		        // TODO can I use getMouseData here and recall it in line 67?
	            mouseY = e.originalEvent.pageY - containerData.yPosition;

			    // find first item below mouse position
	            targetId = '';
		        currentValue = 0;
	            $.each(itemsY, function(index, value) {
		            if( value < mouseY && value > currentValue ){
			            currentValue = value;
			            targetId = index;
		            }
	            });
	            target = $('#'+targetId);

			    // start moving the placeholder
                if(targetId != placeholder.attr('id')){

	                mouseData = getMouseData(e);

                    childrenLength = target.children('ul').children('li').not('.placeholder').length;

                    // CONDITION 1: target has children
                    // RESULT 1: insert placeholder as first child
                    if(childrenLength > 0)
                    {
	                    placeholder.prependTo(target.children('ul'));
                    }

                    // CONDITION 2: target has not children
                    else{

	                    // calculate the gap between mouse x position and placeholder
                        mouseGap = mouseData.xPosition - mouseStartData.xPosition + (placeholderLevel + 1 )*ulMargin;

                        // CONDITION 2.1: target is the last child or the next and last sibling is the placeholder
                        if(
                            $(target).is(':last-child') ||
                            (
                                $(target).is(':not(:last-child)') &&
                                $(target).next().hasClass('placeholder') &&
                                $(target).next().is(':last-child')
                            )
                        ){
                            // calculate target depth and inverse based on mouse x position
                            targetLevel = target.parentsUntil(container,'li').length;
                            parentLevel = Math.round(mouseGap / (ulMargin*2));
                            inverseParentLevel = targetLevel - parentLevel;

	                        // continue only if parent level is bigger than 0
                            if( inverseParentLevel <= targetLevel+1 ){

	                            // CONDITION 2.1.1: target is a grandparent of current target
                                if( inverseParentLevel > 1 ){

	                                // RESULT 2: insert placeholder as sibling of the grandparent
                                    if(placeholder.next().length == 0){
	                                    placeholder.insertAfter(target.parents('li').eq(inverseParentLevel-2));
                                    }

                                }

                                // CONDITION 2.1.2: target is the first parent
                                // RESULT 3: insert placeholder as sibling of the parent
                                else if( inverseParentLevel == 1 ){
	                                placeholder.insertAfter(target);
                                }

                                // CONDITION 2.1.3: target is the target itself
                                // RESULT 4: insert placeholder as child of the target
                                else{
	                                placeholder.prependTo(target.children('ul'));


                                }
                            }
                        }

                        // CONDITION 2.2: target is not the last child
                        else{

	                        // CONDITION 2.2.1: mouse gap is greater than destination x position
                            // RESULT 5: insert placeholder as first child
                            if( mouseGap > ulMargin ){
                                placeholder.prependTo(target.children('ul'));
                            }

                            // CONDITION 2.2.2: mouse gap is lower than destination x position
                            // RESULT 6: insert placeholder as sibling
                            else{
                                placeholder.insertAfter(target);
                            }

                        }
                    }
                }
            }).on('click', '.add', function() {

                var context = ko.contextFor(this);

                bootbox.prompt("Name:", function(result) {
                    if(result){
                        if(result == ''){
                            return false;
                        }

                        childId = Math.floor((Math.random()*100000)+1);
                        childName = result;
                        noDelete = null;
                        noEdit = null;
                        noAdd = null;
                        parentArray = context.$data.tree || context.$data.children;

                        //add a child to the appropriate parent, calling a method off of the main view model (context.$root)
                        context.$root.addChild(childId, childName, noDelete, noAdd, noEdit, parentArray);
                    }

                });



                    /*

                    */
                return false;

            }).on("click", ".edit", function() {
                    li = $(this).closest('li');
                    title = li.find('.sortableTitle:first').html();
                    bootbox.prompt('Edit the title: ' + title + '', 'Cancel', 'OK', function(result) {
                        if (result) {
                            if(result == ''){
                                return false;
                            }
                            li.find('.sortableTitle:first').html(result);
                        }
                    },title);
                    return false;


                }).on("click", ".remove", function() {
                li = $(this).closest('li');
                /*if(li.hasClass('no-delete')){
                    bootbox.alert("You can't delete this node");
                    return false;
                }*/
                title = li.find('.sortableTitle:first').html();
                bootbox.confirm('Are you sure you want to delete ' + title + ' and its children?', function(result) {
                    if (result) {
                        li.remove();
                    }
                });
                return false;

                /*
                //retrieve the context
                var context = ko.contextFor(this),
                parentArray = context.$parent.tree || context.$parent.children;

                //remove the data (context.$data) from the appropriate array on its parent (context.$parent)
			    //context.$root.removeChild(context.$data, parentArray);
                parentArray.remove(context.$data);
			    //console.log(context);

                return false;
                 */

            });

            $('.placeholder').on('drop', function(e) {
                e.stopPropagation();
                placeholders.filter(':visible').after(dragging);
                dragging.trigger('dragend');
                return false;
            });

			/*
			 * I don't need this here
            function getItemsData(items){
                itemsData = {};
                items.each(function(){
                    itemData = getItemData($(this));
                    itemsData[itemData.id] = itemData;
                });
                return itemsData;
            }
            */

            function getItemData(item){
                itemData = {};
                itemPosition = item.position();
                itemData.id = item.attr('id');
                itemData.yPosition = itemPosition.top;
                itemData.xPosition = itemPosition.left;
                return itemData;
            }

            function getMouseData(e){
                mouseData = {};
                mouseData.yPosition = e.originalEvent.pageY - containerData.yPosition;
                mouseData.xPosition = e.originalEvent.pageX - containerData.xPosition;
                return mouseData;
            }

        }
    };

    $.fn.html5NestedSortable = function(method, options) {

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.html5NestedSortable' );
        }

    }



})( jQuery );