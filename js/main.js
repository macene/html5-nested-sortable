$(function() {

    data = [
        {
            "id":"1",
            "noDelete":"true",
            "noAdd":null,
            "noEdit":true,
            "title":"1 - no-delete and no-edit",
            "children":[
                {
                    "id":"2",
                    "title":"1.1",
                    "noDelete":null,
                    "noAdd":null,
                    "noEdit":null,
                    "children":[
                        {
                            "id":"6",
                            "title":"1.1.1",
                            "noDelete":null,
                            "noAdd":null,
                            "noEdit":null,
                            "children":[]
                        }
                    ]
                },
                {
                    "id":"3",
                    "title":"1.2",
                    "noDelete":null,
                    "noAdd":null,
                    "noEdit":null,
                    "children":[]
                }
            ]
        },
        {
            "id":"4",
            "title":"2",
            "noDelete":null,
            "noAdd":null,
            "noEdit":null,
            "children":[]
        },
        {
            "id":"7",
            "title":"3",
            "noDelete":null,
            "noAdd":null,
            "noEdit":null,
            "children":[]
        },
        {
            "id":"5",
            "title":"4",
            "noDelete":null,
            "noAdd":null,
            "noEdit":null,
            "children":[]
        }
    ]

    $('#tree').html5NestedSortable('init', {data:data});



});

