# HTML5 Nested Sortable jQuery plugin

HTML5 Nested Sortable is a jQuery plugin that uses HTML5 drag and drop API to sort nested lists.

This is an experimental plugin. HTML5 drag and drop is not well supported by all the browsers. I am testing it with the last version of Chrome and Firefox.
Please feel free to give suggestions to improve/debug the code.

I use knockout.js templating engine to render the list given a json structure.

## Requirements

jQuery 1.4+
jQuery UI Sortable 1.8+

## Browser Compatibility

Tested with: Firefox, Chrome, Safari

* * *

Author: Manuel Cenedese [http://www.manuelcenedese.com](www.manuelcenedese.com) [@ma_cene](http://twitter.com/ma_cene/)